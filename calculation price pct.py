##加载Python的扩展包
import pymysql
import pandas as pd
from pandas import DataFrame
conn=pymysql.connect(host='120.76.73.21',user='bobby.wang',
                     passwd='mdata2016_10',db='mbm',port=3306)
cur=conn.cursor()#获取一个游标
##连接Mysql数据库，并且从数据库中提取2月29号前的商品ID数据。
##该程序只考虑在这个时间段出现两次及两次以上的商品。
##提取出来的数据为ff格式。整个过程大约花费15分钟。
cur.execute("select c.mdata_goods_id from (SELECT mdata_goods_id,count(*) as Total FROM mbm.mdata_goods_price where created<=1456790399 group by mdata_goods_id) as c where Total>1")
Id_list=cur.fetchall()
##Id_list_df = pd.DataFrame(Id_list,columns='mdata_goods_id')
##计算出向量长度。
numcols = len(Id_list)
##建立一个空数据框，仅包含商品ID、价格、时间。
columns = ["mdata_goods_id", "price", "created"]
df = pd.DataFrame(columns=columns)
##按照之前建立的商品ID列表，按顺序循环提取数据。每次循环提取20个商品的数据，并将数据垂直合并
cur1=conn.cursor(pymysql.cursors.DictCursor)
ntotal=10000
for i in range(0,ntotal,20):
    query="select distinct mdata_goods_id, price,created from mbm.mdata_goods_price where created<=1456790399 and mdata_goods_id in "
    query1=""
    r=ntotal-i
    if r>=20:
        for j in range(20):
            k=i+j
            query1+=str(Id_list[k])
    else:
        for j in range(r):
            k=i+j
            query1+=str(Id_list[k])
    query2=query1.replace(")(","")
    query3=query2.replace(",)",")")
    query4=query+query3
    cur1.execute(query4)
    temp=cur1.fetchall()
    out4=DataFrame(temp,columns=['mdata_goods_id','price','created'])
    df=df.append(out4)
df['date'] = pd.to_datetime(df['created'],unit='s')
##计算价格差及价格降幅
df['id_lag'] = df['mdata_goods_id'].shift(1)
df['price_lag'] = df['price'].shift(1)
df['price_diff']=df['price']-df['price_lag']
df['price_pct']=df['price_diff']/df['price_lag']
##得出每个商品降价幅度的最大的那一条数据，并排序，最后得出前50个商品。
result=df.query('price_pct < 0 & id_lag==mdata_goods_id')
result1=result.sort('price_pct', ascending=True).groupby('mdata_goods_id', as_index=False).first()
Final_result=result1.sort('price_pct', ascending=True)
Final_result_python=Final_result.head(n=50)
cur.close()#关闭游标
cur1.close()#关闭游标
conn.close()#释放数据库资源
##将最后的结果上传到mysql本地库中
con2=pymysql.connect(user='root', passwd='', db='test',host='LocalHost')
Final_result_python.to_sql(con=con2, name='Final_result_python', if_exists='replace', flavor='mysql')
con2.close()

