from lib import mysql_truck,send_mail
import math
from decimal import Decimal
import logging
import pymysql
import time
import os

logging.basicConfig(format="%(asctime)-15s %(message)s")


def init_logger(PATH_LOG, logger_name='default.log'):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('[%(asctime)s] - %(levelname)s -  %(message)s')
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)
    file_handler = logging.FileHandler(os.path.join(PATH_LOG, logger_name))
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    return logger


def open_freelife_server_mf_mall_db_conn():
    conn = pymysql.connect(host='120.25.62.241', user='mf_dba', passwd='mz28sKcx5NN3JQA4', db='mf_mall',
                           port=3306, charset='utf8')
    cur = conn.cursor()
    return conn, cur


def open_spider9_mdata_db_conn():
    conn = pymysql.connect(host='192.168.1.29', user='spider', passwd='1234', db='mdata', port=3306)
    cur = conn.cursor()
    return conn, cur


def close_db(conn, cur):
    cur.close()
    conn.close()


def get_db_table_bulk(select_fields, table_name, lower_limit, upper_limit, cur):
    query = "SELECT {0} FROM `{1}` where seller_product_price_auto_id between {2} and {3};".format(select_fields,
                                                                                                   table_name,
                                                                                                   lower_limit,
                                                                                                   upper_limit)
    cur.execute(query)
    columns = cur.description
    result = []
    for value in cur.fetchall():
        tmp = {}
        for (index, column) in enumerate(value):
            tmp[columns[index][0]] = column
        result.append(tmp)
    return result


def get_db_table(select_fields, table_name, filter,order, cur):
    query = "SELECT {0} FROM `{1}` {2} {3} limit 0, 1000;".format(select_fields, table_name,filter,order)
    cur.execute(query)
    columns = cur.description
    result = []
    for value in cur.fetchall():
        tmp = {}
        for (index, column) in enumerate(value):
            tmp[columns[index][0]] = column
        result.append(tmp)
    return result


def get_goods_price_info_freelife(item):
    if item['seller_product_original_price'] is None:
        item['seller_product_original_price'] = '0.00'
    if item['seller_product_original_msrp'] is None:
        item['seller_product_original_msrp'] = '0.00'
    if item['seller_product_original_currency'] is None:
        item['seller_product_original_currency'] = '0.00'
    if item['seller_product_original_msrp'] != '0.00':
        item['seller_product_current_mrsp'] = str(math.ceil(float(item['seller_product_original_msrp'])
                                                            * float(item['seller_product_original_currency_rate'])))
    else:
        item['seller_product_current_mrsp'] = '0.00'
    item['seller_product_price'] = str(math.ceil(Decimal(float(item['seller_product_price'])
                                                         * float(item['seller_product_original_currency_rate']))))
    item['seller_product_price_history_status'] = ''
    if float(item['seller_product_price']) > 500:
        item['seller_product_shipping_cost'] = '0'
    else:
        item['seller_product_shipping_cost'] = '50'
    item['seller_product_customs_cost'] = ''
    final_data = (item['seller_product_price_history_uuid'],
                  item['seller_product_uuid'],
                  item['seller_product_price'],
                  item['seller_product_current_mrsp'],
                  str(item['seller_product_price_time']),
                  item['seller_product_original_price'],
                  item['seller_product_original_msrp'],
                  item['seller_product_original_currency'],
                  item['seller_product_original_currency_rate'],
                  item['seller_product_shipping_cost'],
                  item['seller_product_customs_cost'],
                  item['seller_product_price_history_status'],
                  item['is_modified'])
    return final_data


def insert_into_or_update_db(data, con, cursor):
    insert_query = "INSERT INTO freelife_mdata_price_history (" \
                   "seller_product_price_history_uuid, " \
                   "seller_product_uuid, " \
                   "seller_product_price, " \
                   "seller_product_current_mrsp, " \
                   "seller_product_price_time, " \
                   "seller_product_original_price, " \
                   "seller_product_original_mrsp, " \
                   "seller_product_original_currency, " \
                   "seller_product_original_currency_rate, " \
                   "seller_product_shipping_cost, " \
                   "seller_product_customs_cost, " \
                   "seller_product_price_history_status, " \
                   "is_modified) " \
                   "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" \
                   "ON DUPLICATE KEY UPDATE " \
                   "seller_product_uuid = VALUES(seller_product_uuid), " \
                   "seller_product_price = VALUES(seller_product_price), " \
                   "seller_product_current_mrsp = VALUES(seller_product_current_mrsp), " \
                   "seller_product_price_time = VALUES(seller_product_price_time), " \
                   "seller_product_original_price = VALUES(seller_product_original_price), " \
                   "seller_product_original_mrsp = VALUES(seller_product_original_mrsp), " \
                   "seller_product_original_currency = VALUES(seller_product_original_currency), " \
                   "seller_product_shipping_cost = VALUES(seller_product_shipping_cost);"
    cursor.executemany(insert_query, data)
    con.commit()


def data_extractor(select_fields, mysql_table, filter, order, cursor, chunk):
    try:
        data = get_db_table(select_fields, mysql_table, filter,order, cursor)
    except Exception as error:
        if "gone away" in str(error):
            logger.info("Retry To Retrive Data From " + mysql_table + " Between " + str(chunk * 1000) + " And " + str(
                chunk * 1000 + 1000))
            conn, cursor = open_spider9_mdata_db_conn()
            data = get_db_table(select_fields, mysql_table, filter,order, cursor)
            logger.info("Successfully Retry To Retrive Data From " + mysql_table + " Between " + str(
                chunk * 1000) + " And " + str(chunk * 1000 + 1000))
        else:
            raise error
    return data


def data_transformator(data):
    try:
        final_data = []
        for item in data:
            temp = get_goods_price_info_freelife(item)
            final_data.append(temp)
    except Exception as error:
        raise error
    return final_data


def data_uploader(final_data, connection_freelife_server, cursor_freelife_server):
    try:
        insert_into_or_update_db(final_data, connection_freelife_server, cursor_freelife_server)
    except Exception as error:
        if "gone away" in str(error):
            logger.info("Retry To Upload Data")
            connection_freelife_server, cursor_freelife_server = open_freelife_server_mf_mall_db_conn()
            insert_into_or_update_db(final_data, connection_freelife_server, cursor_freelife_server)
            logger.info("Successfully Retry To Upload Data")
        else:
            raise error


# upload data
def bulk_etl_between_db_tables(select_fields, count_sql, filter,order, mysql_table, db_cfg, max_update_time,
                                  max_id):
    mysqltruck = mysql_truck.MysqlTruck(user=db_cfg['user'], password=db_cfg['password'], db=db_cfg['db'],
                                        host=db_cfg['host'])
    count_value = mysqltruck.execute_select_sql(count_sql.format(mysql_table, filter), True)[0]["count(*)"]
    if count_value > 0 and max_id != '0':
        logger.info("Start To Sync Freelife Price History From Mdata To Freelife")
        chunks = math.ceil(float(count_value) / 1000)
        conn, cursor = open_spider9_mdata_db_conn()
        connection_freelife_server, cursor_freelife_server = open_freelife_server_mf_mall_db_conn()
        for chunk in range(0, chunks):
            try:
                # fetch data from mysql
                logger.info("Start : Retrive Data From " + mysql_table + " Between " + str(chunk * 1000) + " And " +
                            str(chunk * 1000 + 1000))
                data = data_extractor(select_fields, mysql_table, filter,order, cursor, chunk)
                logger.info("Finish : Retrive Data From " + mysql_table + " Between " + str(chunk * 1000) + " And " +
                            str(chunk * 1000 + 1000))
                final_data =data_transformator(data)
                logger.info("Start To Upload Data From " + mysql_table + " Between " + str(chunk * 1000) + " And " +
                            str(chunk * 1000 + 1000))
                data_uploader(final_data, connection_freelife_server, cursor_freelife_server)
                max_update_time = str(data[-1]['update_time'])
                max_id = data[-1]['seller_product_price_history_uuid']
                filter=" where update_time > '" + max_update_time + "' or (update_time = '" + max_update_time \
                       + "' and seller_product_price_history_uuid > '" + max_id + "') order by update_time," \
                       "seller_product_price_history_uuid"
                logger.info("Finish To Upload Data From " + mysql_table + " Between " + str(chunk * 1000)+ " And " +
                            str(chunk * 1000 + 1000))
            except Exception as error:
                logger.error(error)
                send_mail.send(["bobby.wang@malama.ca"], ["bob.feng@malama.ca"],
                               "Sync Freelife Price Mdata-Freelife Fatal Exception: Immediate Action Required",
                               str(error))
                logger_time.error("New Update Time: " + max_update_time + " New ID: " + max_id)
                raise error
        logger.info("Finish To Sync Freelife Price History From Mdata To Freelife")
        close_db(conn, cursor)
        close_db(connection_freelife_server, cursor_freelife_server)
        return max_update_time,max_id
    elif count_value > 0 and max_id == '0':
        logger.info("Start To Sync Freelife Price History From Mdata To Freelife")
        chunks = math.ceil(float(count_value) / 1000)
        conn, cursor = open_spider9_mdata_db_conn()
        connection_freelife_server, cursor_freelife_server = open_freelife_server_mf_mall_db_conn()
        for chunk in range(0, chunks):
            try:
                # fetch data from mysql
                logger.info(
                    "Start : Retrive Data From " + mysql_table + " Between " + str(chunk * 1000) + " And " + str(
                        chunk * 1000 + 1000))
                try:
                    data = get_db_table_bulk(select_fields, mysql_table, (chunk * 1000 + 1), ( chunk * 1000 + 1000),
                                             cursor)
                except Exception as error:
                    if "gone away" in str(error):
                        logger.info("Retry To Retrive Data From " + mysql_table + " Between " + str(chunk * 1000) +
                                    " And " + str(chunk * 1000 + 1000))
                        conn, cursor = open_spider9_mdata_db_conn()
                        data = get_db_table_bulk(select_fields, mysql_table, (chunk * 1000 + 1), (chunk * 1000 + 1000),
                                                 cursor)
                        logger.info("Successfully Retry To Retrive Data From " + mysql_table + " Between " + str(
                                    chunk * 1000) + " And " + str(chunk * 1000 + 1000))
                    else:
                        raise error
                logger.info("Finish : Retrive Data From " + mysql_table + " Between " + str(chunk * 1000) + " And " +
                            str( chunk * 1000 + 1000))
                final_data = data_transformator(data)
                logger.info("Start To Upload Data From " + mysql_table + " Between " + str(chunk * 1000) + " And " +
                            str(chunk * 1000 + 1000))
                data_uploader(final_data,connection_freelife_server, cursor_freelife_server)
                logger.info("Finish To Upload Data From " + mysql_table + " Between " + str(chunk * 1000) + " And " +
                            str(chunk * 1000 + 1000))
            except Exception as error:
                 logger.error(error)
                 send_mail.send(["bobby.wang@malama.ca"], ["bob.feng@malama.ca"],
                                "Sync Freelife Price Mdata-Freelife Fatal Exception: Immediate Action Required",
                                str(error))
                 raise error
        logger.info("Finish To Sync Freelife Price History From Mdata To Freelife")
        close_db(conn, cursor)
        close_db(connection_freelife_server, cursor_freelife_server)
    else:
        logger.info("No Need To Upload Data To Freelife !")
        return max_update_time,max_id


logger = init_logger(PATH_LOG='E:/Projects/back_end/import_data/freelife',
                     logger_name='sync_freelife_price_history_mdata_to_freelife.log')
logger_time = init_logger(PATH_LOG='E:/Projects/back_end/import_data/freelife',
                          logger_name='sync_freelife_price_history_mdata_to_freelife_update_time.log')
select_fields_price = "seller_product_price_history_uuid," \
                      "seller_product_uuid," \
                      "seller_product_price," \
                      "seller_product_price_time," \
                      "seller_product_original_price," \
                      "seller_product_original_msrp," \
                      "seller_product_original_currency," \
                      "'7.5' as seller_product_original_currency_rate, " \
                      "'0' as is_modified," \
                      "update_time"
max_update_time = '2016-07-21 21:17:05'
max_id = '0'
while True:
    max_update_time,max_id = bulk_etl_between_db_tables(
        select_fields=select_fields_price,
        count_sql="select count(*) from {0} {1};",
        filter=" where update_time > '" + max_update_time + "' or (update_time = '" +
               max_update_time + "' and seller_product_price_history_uuid > '"
               + max_id +"') ",
        order="order by update_time,seller_product_price_history_uuid",
        mysql_table='seller_product_price_history',
        db_cfg={'user': 'spider', 'password': '1234', 'db': 'mdata', 'host': '192.168.1.29'},
        max_update_time=max_update_time,
        max_id=max_id)
    logger_time.info("New Update Time: " + max_update_time + "New Update Goods Price History Uuid: " + max_id)
    logger.info("Start To Sleep 2 Hours !")
    time.sleep(7200)
    logger.info("Finish To Sleep 2 Hours !")